﻿/*
* command_pseudo.java
*
* Pseudo-Java code implementing a command pattern
* for the application 'Impfportal'.
*
* Ida Welle, Michael Zent
*
* Software Engineering
* Lecturer: Barry Linnert
* Tutor: Tamara Fischer
* SS 2021 FU Berlin
*/

public abstract class Command
    { public abstract void execute(); }

// ConcreteCommand
public class TerminVereinbarung extends Command
{
    private Terminserver ts;
    private Buchungscode bc;
    private Datum datum;

    public TerminVereinbarung(Terminserver ts, Buchungscode bc)
    {
        this.ts = ts;
        this.bc = bc;
    }

    public wunsch(Datum datum) { this.datum = datum; }

    @Override
    public void execute()
    {
        ts.terminBelegen(bc, datum);
    }
}

// Invoker
public class Knopf
{
    public Knopf() { /* … */ }

    public void klick(Command cmd)
    {
        cmd.execute();
    }
}

// Receiver
public class Terminserver
{
    public Terminserver() { /* … */}

    public void terminBelegen(Buchungcode bc, Datum datum)
    {
        // Prüfe ob das Datum für einen Termin frei ist
        // Trage in Datenbank <bc, datum> ein
    }
    
    public void terminFreigeben(Buchungcode bc)
    {
        // Analoges...
    }
}

// Client
public class Impfterminportal
{
    private static Buchungscode bc = /* … */;
    private static Terminserver ts = new Terminserver();
    private static TerminVereinbarung tv = new TerminVereinbarung(ts,bc);

    public static void main(String[] args)
    {
        Datum datum = /* … */;
        tv.wunsch(datum);
        Knopf knopf = new Knopf();
        knopf.klick(TerminVereinbarung);
    }
}
